import React, { Component } from 'react';

import './Sidebar.css';

class Sidebar extends Component {
  render() {
    return (
      <div className="Sidebar">
        <h1>Foo</h1>
        <ul>
          <li><a href="#">Monsters</a></li>
          <li><a href="#">Upgrades</a></li>
          <li><a href="#">Quests</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
