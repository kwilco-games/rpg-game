import React, { Component } from 'react';
import './App.css';

import Sidebar from './Sidebar.js'
import Body from './Body.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Sidebar />
        <Body />
      </div>
    );
  }
}

export default App;
